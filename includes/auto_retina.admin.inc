<?php
/**
 * @file
 * Provides administration functions for the auto_retina module.
 *
 * @ingroup auto_retina
 * @{
 */

/**
 * Form builder. Configure settings for auto_retina.
 *
 * @ingroup forms
 * @see     system_settings_form().
 */
function auto_retina_admin_settings($form, &$form_state) {

  $form['auto_retina_suffix'] = array(
    '#type' => 'textfield',
    '#title' => t('Retina filename suffix'),
    '#description' => t('This suffix appears at the end of a filename, before the dot+extension to indicate it is the retina version of an image, e.g. "@2x".  <strong>To allow more than one multiplier, add a space-separated list of suffixes, e.g. "@.75x @1.5x @2x @3x"</strong>'),
    '#default_value' => variable_get('auto_retina_suffix', AUTO_RETINA_SUFFIX),
    '#required' => TRUE,
  );

  if (module_exists('image_style_quality')) {
    $explanation = t('If a given style includes an image style quality effect it will be used as the basis, if not then the <a href="!url">image toolkit setting</a> is used.  ', [
      '!url' => url('admin/config/media/image-toolkit', array('query' => drupal_get_destination())),
    ]);
  }
  else {
    $explanation = t('The basis is the <a href="!url">image toolkit setting</a>. <em>(Consider installing the <a href="!module" target="_blank">Image Style Quality</a> module for more options.)</em>  ', [
      '!url' => url('admin/config/media/image-toolkit', array('query' => drupal_get_destination())),
      '!module' => url('https://www.drupal.org/project/image_style_quality'),
    ]);
  }

  $form['auto_retina_quality_multiplier'] = array(
    '#title' => t('JPEG Quality Multiplier (for magnified images only)'),
    '#type' => 'textfield',
    '#element_validate' => array('element_validate_number'),
    '#field_suffix' => 'x JPEG percentage',
    '#size' => 4,
    '#description' => t('Affect the JPEG quality used when generating the magnified image(s).  !explanation<strong>If the basis is 80% and you leave this at 1, the retina image will be generated at 80% (no change); however if you set this to .5 then the retina image will be generated at 40%.</strong>  The lower the percentage multiplier, the smaller will be the file size of the retina image, <em>at the expense of quality</em>.', array(
      '!explanation' => $explanation,
    )),
    '#default_value' => variable_get('auto_retina_quality_multiplier', 1),
  );

  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['advanced']['auto_retina_regex'] = array(
    '#type' => 'textfield',
    '#title' => t('Retina filename regex'),
    '#description' => t('Enter a regex expression to use for determining if an url is retina.  The token <code>[suffix]</code> may be used to dynamically populate the setting from above. You may omit start/end delimiters.'),
    '#default_value' => variable_get('auto_retina_regex', AUTO_RETINA_REGEX),
    '#required' => TRUE,
  );

  $form['advanced']['auto_retina_js'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include the javascript settings <code>Drupal.settings.autoRetina</code> on every page?'),
    '#default_value' => variable_get('auto_retina_js', AUTO_RETINA_JS),
  );

  return system_settings_form($form);
}
