<?php
/**
 * @file
 * Provides alterations to effects for retina processing.
 *
 * @ingroup auto_retina
 */

/**
 * Implements hook_auto_retina_effect_alter().
 *
 * Calculates the correct retina size for $effect.
 */
function auto_retina_auto_retina_effect_alter(&$effect, $retina_info, $context) {
  if (!empty($effect['data']['width'])) {
    list(, $original) = $context;
    $original_info = image_get_info($original);

    // We always upscale.  The maximum value to upscale to will be optimized.
    $effect['data']['upscale'] = TRUE;

    // Optimize the retina image size.
    $data = $effect['data'] + ['height' => NULL];
    $optimized = _auto_retina_optimize_image_size($data['width'], $data['height'], $retina_info['multiplier'], $original_info['width']);
    $effect['data']['width'] = $optimized['width'];
    $effect['data']['height'] = $optimized['height'];

    // Make a log entry if the image is of lower quality.
    if ($optimized['is_suboptimum'] && variable_get('auto_retina_log', AUTO_RETINA_LOG)) {
      watchdog('auto_retina', 'Poor retina quality, @percentage% of ideal.  To fix this, you must upload a new version of %file at least @width pixels wide.', array(
        '%file' => $original,
        '@width' => $optimized['optimum_width'],
        '@percentage' => $optimized['percent_of_optimum'],
      ), $optimized['percent_of_optimum'] < 50 ? WATCHDOG_WARNING : WATCHDOG_NOTICE);
    }
  }
}

/**
 * Optimize the style effect for our magnified image.
 *
 * @param int $effect_width
 *   The target width of the effect. This is used as the base for magnification.
 * @param int|null $effect_height
 *   The target height or null.
 * @param int|float $magnification
 *   The magnification value.
 * @param int $source_width
 *   The native width of the original source image.
 *
 * @return array
 *   - optimum_width int
 *   - is_suboptimum bool
 *   - percent_of_optimum int
 */
function _auto_retina_optimize_image_size($effect_width, $effect_height, $magnification, $source_width) {

  // Determine if we need to cap our retina width due to poor quality of original.
  $max_width = NULL;
  if ($source_width < $effect_width) {
    $max_width = $effect_width;
  }
  elseif (!($magnification * $effect_width) <= $source_width) {
    $max_width = $source_width;
  }

  if (!empty($effect_height)) {
    $data = auto_retina_multiply_width_maintain_aspect_ratio($magnification, $effect_width, $effect_height, $max_width);
    $optimum_retina_width = $data['optimum_retina_width'];
  }
  else {
    $effect_width *= $magnification;
    $optimum_retina_width = $effect_width;
    if ($max_width) {
      $effect_width = min($effect_width, $max_width);
    }
  }

  return [
    'width' => $effect_width,
    'height' => $effect_height,
    'optimum_width' => intval($optimum_retina_width),
    'is_suboptimum' => $effect_width < $optimum_retina_width,
    'percent_of_optimum' => intval($effect_width * 100 / $optimum_retina_width),
  ];
}
