---
use_twig: true
---
# Drupal Module: Auto Retina
**Author:** Aaron Klump  <sourcecode@intheloftstudios.com>

You may also visit the [project page](http://www.drupal.org/project/auto_retina) on Drupal.org.

##Summary
{% include('_overview.md') %}

##Contact
* **In the Loft Studios**
* Aaron Klump - Developer
* PO Box 29294 Bellingham, WA 98228-1294
* _skype_: intheloftstudios
* _d.o_: aklump
* <http://www.InTheLoftStudios.com>
