# Frequently Asked Questions

## What is the default behaviour when images are not 2x the default image style width?

The image native width will not be exceeded.  So if a 2x style calculates the width at 200px, but the original image is only 150px wide, then the generated retina image will only be 150px wide.
