<?php

/**
 * @file
 * PHPUnit tests for the AutoRetina class
 */

require_once dirname(__FILE__) . '/../../auto_retina.module';

function variable_get($name, $default) {
  return $default;
}

class AutoRetinaTest extends \PHPUnit_Framework_TestCase {

  /**
   * Provides data for test_auto_retina_auto_retina_effect_alter.
   *
   * Each element provides this data:
   *
   * - control int
   * - effect_width int
   * - effect_height int|null
   * - multiplier float|int
   * - original_width int
   */
  public function dataForTest_auto_retina_auto_retina_effect_alterProvider() {
    $tests = array();

    // The standard was not upscaled, but original is too small.
    $tests[] = array(
      [360, NULL, 360],
      240,
      NULL,
      1.5,
      800,
    );

    // The standard was not upscaled, but original is too small.
    $tests[] = array(
      [400, NULL, 640],
      320,
      NULL,
      2,
      400,
    );

    // The standard was upscaled, therefor the retina is same as derivate.
    $tests[] = array(
      [320, NULL, 640],
      320,
      NULL,
      2,
      300,
    );

    // Was not upscaled and the origina is larger than optimium retina width.
    $tests[] = array(
      [1920, NULL, 3200],
      320,
      NULL,
      10,
      1920,
    );

    return $tests;
  }

  /**
   * @dataProvider dataForTest_auto_retina_auto_retina_effect_alterProvider
   */
  public function test_auto_retina_auto_retina_effect_alter($control, $effect_width, $effect_height, $magnification, $original_width) {
    $optimized = _auto_retina_optimize_image_size($effect_width, $effect_height, $magnification, $original_width);
    $this->assertEquals($control[0], $optimized['width']);
    $this->assertEquals($control[1], $optimized['height']);
    $this->assertEquals($control[2], $optimized['optimum_width']);
    $this->assertEquals($control[2] > $optimized['width'], $optimized['is_suboptimum']);
    $this->assertEquals(intval($optimized['width'] * 100 / $optimized['optimum_width']), $optimized['percent_of_optimum']);
  }

  /**
   * Provides data for testImageStyleAddRetinaSettings.
   */
  function DataForTestImageStyleAddRetinaSettingsProvider() {
    $tests = array();

    $tests[] = array(
      'photo@2x.jpg',
      'photo.jpg',
      array('suffix' => '@2x', 'multiplier' => 2, 'quality_multiplier' => 1),
    );

    $tests[] = array(
      'photo.jpg',
      'photo.jpg',
      array('suffix' => '', 'multiplier' => 1, 'quality_multiplier' => 1),
    );

    return $tests;
  }

  /**
   * @dataProvider DataForTestImageStyleAddRetinaSettingsProvider
   */
  public function testImageStyleAddRetinaSettings($uri_in, $uri_out, $style_out) {
    $style = array();
    $uri = $uri_in;
    $original_url = auto_retina_image_style_prepare($style, $uri);
    $this->assertSame($uri_in, $uri);
    $this->assertSame($uri_out, $original_url);
    $this->assertSame(array('auto_retina' => $style_out), $style);
  }

  /**
   * Provides data for testBuildRetinaPath.
   */
  function DataForTestBuildRetinaPathProvider() {
    $tests = array();

    $tests[] = array(
      '/do/re/mi/photo.jpg',
      '/do/re/mi/photo@2x.jpg',
    );
    $tests[] = array(
      'photo.jpg',
      'photo@2x.jpg',
    );
    $tests[] = array(
      'photo@2x.jpg',
      'photo@2x.jpg',
    );

    return $tests;
  }

  /**
   * @dataProvider DataForTestBuildRetinaPathProvider
   */
  public function testBuildRetinaPath($subject, $control) {
    return $this->assertSame($control, auto_retina_get_retina_path($subject));
  }

}

